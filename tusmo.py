import sys
import os


def check_if_match(parsed_word, parsed_word_to_find, must_contain: list):
    """
    check if two given list can match
    :return:
    """
    dot: str = "."
    match: bool = True

    not_dot_value = []

    # get index if dot or not
    for i in range(0, len(parsed_word_to_find)):
        if parsed_word_to_find[i] is not dot:
            not_dot_value.append(i)

    # check letter match
    for i in range(0, len(parsed_word)):
        current_letter = parsed_word[i]
        current_letter_to_find = parsed_word_to_find[i]

        if i in not_dot_value:
            if current_letter != current_letter_to_find:
                match = False
                break
        elif must_contain is not None:
            for letter in must_contain:
                if letter not in parsed_word:
                    match = False
                    break

    return match


def find(language: str, word_to_find: str, must_contain: str = None):
    print(f"the word to find: {word_to_find} in {language}")

    matching_words = []

    with open(f"dictionary/dictionary_{language}.txt", "r") as f:
        # check each word in file
        for word in f.readlines():
            word = word.strip()
            # check same length
            if len(word) == len(word_to_find):
                if check_if_match(list(word), list(word_to_find), list(must_contain) if must_contain else None):
                    matching_words.append(word)

    # remove duplicate values
    matching_words = list(dict.fromkeys(matching_words))
    print(matching_words)


# sys.argv[1] is func name
# sys.argv[2] is the arg
globals()[sys.argv[1]](sys.argv[2], sys.argv[3], sys.argv[4] if len(sys.argv) == 5 else None)
