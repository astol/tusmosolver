# TUSMO SOLVER

## Description
This project is about to find for you word you are seeking for while you are playing tusmo

## Compatibility
| Language | Supported |
|----------|-----------|
| French   | yes       |
| English  | yes       |
| German   | yes       |
| Spanish  | yes       |

## How to use
```shell
python tusmo.py find fr yourword must_contain
```
- _fr_ set language to use (required)
- _yourword_ must be format like this **b.n.n.** for word banana with "a" letter missing (required)
- _must_contain_ set all your letter bad placed (optional)